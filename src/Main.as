package 
{
	import flash.desktop.Clipboard;
	import flash.desktop.ClipboardFormats;
	import flash.desktop.NativeDragManager;
	import flash.display.InteractiveObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.NativeDragEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.FileFilter;
	import flash.net.FileReference;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	import flash.utils.Endian;
	
	/**
	 * SWF混淆工具
	 * @author WLDragon 2016-01-04
	 */
	public class Main extends Sprite 
	{
		private var swfArea:Sprite = new Sprite();
		private var folderArea:Sprite = new Sprite();
		
		private var mixer:MixData;
		private var getter:GetMixFields = new GetMixFields();
		
		private var stream:FileStream = new FileStream();
		private var swfPath:String;
		
		public function Main():void 
		{
			mixer = new MixData(saveSwf);
			
			swfArea.x = swfArea.y = 10;
			swfArea.graphics.beginFill(0x00DD37);
			swfArea.graphics.drawRoundRect(0,0,100,100,30,30);
			swfArea.graphics.endFill();
			addChild(swfArea);
			
			folderArea.x = 120;
			folderArea.y = 10;
			folderArea.graphics.beginFill(0x1463F5);
			folderArea.graphics.drawRoundRect(0,0,100,100,30,30);
			folderArea.graphics.endFill();
			addChild(folderArea);
			
			swfArea.addEventListener(NativeDragEvent.NATIVE_DRAG_ENTER, onDragEnter);
			swfArea.addEventListener(NativeDragEvent.NATIVE_DRAG_DROP, onDragDrop);
			folderArea.addEventListener(NativeDragEvent.NATIVE_DRAG_ENTER, onDragEnter);
			folderArea.addEventListener(NativeDragEvent.NATIVE_DRAG_DROP, onDragDrop);
		}
		
		private function onDragDrop(e:NativeDragEvent):void 
		{
			var cb:Clipboard = e.clipboard;
			if (cb.hasFormat(ClipboardFormats.FILE_LIST_FORMAT)) {
				var a:Array = cb.getData(ClipboardFormats.FILE_LIST_FORMAT) as Array;
				if (a != null) {
					var f:File = a[0];
					if (e.target == folderArea) {
						if(f.isDirectory) getter.begin(f.getDirectoryListing(), saveMixs);//提取字段
					}else {
						if (f.name.indexOf(".swf") > -1) {
							swfPath = f.nativePath;
							//混淆swf，每次混淆都重新提取混淆库
							var mixsFile:File = new File(File.applicationDirectory.nativePath + "/mixs.txt");
							if (mixsFile.exists) {
								stream.open(mixsFile, FileMode.READ);
								var s:String = stream.readUTFBytes(stream.bytesAvailable);
								stream.close();
								
								a = s.split(",");
								
								var nm:File = new File(File.applicationDirectory.nativePath + "/nomixs.txt");
								if (nm.exists) {
									stream.open(nm, FileMode.READ);
									var b:Array = stream.readUTFBytes(stream.bytesAvailable).split(",");
									stream.close();
									
									for (var i:int = b.length - 1; i >= 0; i--){
										var j:int = a.indexOf(b[i]);
										if (j > -1) a.splice(j, 1);
									}
								}
							}	
							mixer.reset();
							mixer.mixStrings = a;
							stream.open(f, FileMode.READ);
							var byte:ByteArray = new ByteArray();
							stream.readBytes(byte);
							stream.close();
							mixer.mix(byte);
						}
					}
				}
			}
		}
		
		private function onDragEnter(e:NativeDragEvent):void 
		{
			NativeDragManager.acceptDragDrop(e.currentTarget as InteractiveObject);
		}
		
		private function saveMixs(str:String):void 
		{
			stream.open(new File(File.applicationDirectory.nativePath + "/mixs.txt"), FileMode.WRITE);
			stream.writeUTFBytes(str);
			stream.close();
		}
		
		private function saveSwf(data:ByteArray, table:String):void
		{
			stream.open(new File(swfPath.replace(".swf","") + "_mix.swf"), FileMode.WRITE);
			stream.writeBytes(data);
			stream.close();
			stream.open(new File(swfPath.replace(".swf","") + "_table.txt"), FileMode.WRITE);
			stream.writeUTFBytes(table);
			stream.close();
		}
	}
}